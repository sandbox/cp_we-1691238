(function ($) {
	var ports = {
		ftp: 21,
		sftp: 22
	};

	var set_port = function () {
		if ( $('input[name=port]').val()=='22' || $('input[name=port]').val()=='21' || !$('input[name=port]').val() ) {
			$('input[name=port]').val(ports[$('select[name=type]').val()]);
		}
	}

	var init_all = function () {
		$('select[name=type]').change(set_port);
	}

	$(document).ready(init_all);
})(jQuery);